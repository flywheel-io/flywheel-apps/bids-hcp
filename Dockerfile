# Creates docker container that runs HCP Pipeline algorithms
#
#
# Uses focal 20.04 LTS
FROM flywheel/hcp-base:1.0.3_4.3.0

LABEL maintainer="Flywheel <support@flywheel.io>"

# Remove expired LetsEncrypt cert
RUN update-ca-certificates
ENV REQUESTS_CA_BUNDLE "/etc/ssl/certs/ca-certificates.crt"

# Install BIDS Validator
RUN apt-get update && \
    curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
    apt-get install -y --no-install-recommends\
    zip \
    nodejs \
    tree \
    git && \
    rm -rf /var/lib/apt/lists/* && \
    npm install -g bids-validator@1.9.0 && \
    rm -rf $HOME/.npm

#try to reduce strangeness from locale and other environment settings
ENV LC_ALL=C \ 
    LANGUAGE=C
#POSIXLY_CORRECT currently gets set by many versions of fsl_sub, unfortunately, but at
# least don't pass it in if the user has it set in their usual environment
RUN unset POSIXLY_CORRECT

#############################################
# MSM_HOCR v3 binary is installed in base image.
ENV MSMBINDIR=${HCPPIPEDIR}/MSMBinaries

# Patch from Keith Jamison's Gear. Change at your own risk.
COPY scripts/patch/DiffPreprocPipeline.sh /opt/HCP-Pipelines/DiffusionPreprocessing/

## Fix libz error
#RUN ln -s -f /lib/x86_64-linux-gnu/libz.so.1.2.11 /opt/workbench/libs_linux64/libz.so.1
#
## Fix libstdc++6 error
#RUN ln -sf /usr/lib/x86_64-linux-gnu/libstdc++.so.6.0.24 /opt/matlab/v92/sys/os/glnxa64/libstdc++.so.6

# Add poetry oversight.
RUN apt-get update &&\
    apt-get install -y --no-install-recommends \
	software-properties-common &&\
	add-apt-repository -y 'ppa:deadsnakes/ppa' &&\
	apt-get update && \
	apt-get install -y --no-install-recommends python3.9\
    python3.9-dev \
	python3.9-venv \
	python3-pip &&\
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install poetry based on their preferred method. pip install is finnicky.
# Designate the install location, so that you can find it in Docker.
ENV PYTHONUNBUFFERED=1 \
    POETRY_VERSION=1.8.3 \
    # make poetry install to this location
    POETRY_HOME="/opt/poetry" \
    # do not ask any interactive questions
    POETRY_NO_INTERACTION=1 \
    VIRTUAL_ENV=/opt/venv
RUN python3.9 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN python3.9 -m pip install --upgrade pip && \
    ln -sf /usr/bin/python3.9 /opt/venv/bin/python3
ENV PATH="$POETRY_HOME/bin:$PATH"

# get-poetry respects ENV
RUN curl -sSL https://install.python-poetry.org | python3 - && \
    chmod a+x $(which poetry)

# Installing main dependencies
ARG FLYWHEEL=/flywheel/v0

COPY pyproject.toml poetry.lock $FLYWHEEL/
RUN poetry install --only main

COPY scripts ${FLYWHEEL}/utils/scripts
COPY scenes ${FLYWHEEL}/utils/scenes

## Installing the current project (most likely to change, above layer can be cached)
## Note: poetry requires a README.md to install the current project
COPY run.py manifest.json README.md $FLYWHEEL/
COPY fw_gear_bids_hcp $FLYWHEEL/fw_gear_bids_hcp

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py && \
    echo "hcp-gear" > /etc/hostname

ENTRYPOINT ["poetry","run","python","/flywheel/v0/run.py"]
