#!/usr/bin/env python3
import logging
import os
import sys
from pathlib import Path

from flywheel_gear_toolkit import GearToolkitContext

# from flywheel_gear_toolkit.hpc import freesurfer_utils, singularity
# from flywheel_gear_toolkit.hpc import singularity
from flywheel_gear_toolkit.licenses.freesurfer import install_freesurfer_license

from fw_gear_bids_hcp import tmp_freesurfer_utils, tmp_singularity
from fw_gear_bids_hcp.hcp_diff import diff_main
from fw_gear_bids_hcp.hcp_func import func_main
from fw_gear_bids_hcp.hcp_struct import struct_main
from fw_gear_bids_hcp.utils import gear_arg_utils, helper_funcs
from fw_gear_bids_hcp.utils.bids import bids_file_locator
from fw_gear_bids_hcp.utils.set_gear_args import GearArgs

log = logging.getLogger(__name__)


def main(gtk_context: GearToolkitContext) -> int:
    # Set up the templates, config options from the config_json, and other essentials
    log.info("Populating gear arguments")
    gear_args = GearArgs(gtk_context)

    # Run all the BIDS-specific downloads and config settings
    log.info("Locating BIDS structure.")
    bids_info = bids_file_locator.bidsInput(gtk_context)
    bids_info.find_bids_files(gear_args)

    if bids_info.error_count > 0:
        log.info("Please carefully check the error messages to correct " "your dataset before retrying the gear.")
        sys.exit(1)
    else:
        e_code = 0
    # Structural analysis
    if any("surfer" in arg.lower() for arg in [gear_args.common["stages"]]):
        if not gear_args.structural["avgrdcmethod"] == "NONE":
            # Add distortion correction information
            gear_args.structural.update(helper_funcs.set_dcmethods(gear_args, bids_info.layout, "structural"))
        e_code += struct_main.run(gear_args)
    elif not gear_args.fw_specific["gear_dry_run"]:
        # If the analysis has been done piecemeal, then the previous struct zip must be specified
        # It is not efficient to search for all previous analyses and choose one of the structural zips.
        # This issue is particularly relevant to multiple analyses with different parameters. Choosing
        # one basically blindly is not advisable for FlyWheel.

        # Structural stage ends with zipping file. If that stage is complete, either from prior processing
        # or from current run, there should be a zip file to grab.
        gear_arg_utils.process_hcp_zip(gear_args.common["hcpstruct_zip"])

    # hcpstruct_zip is available when FS runs or when starting with a structural zip
    # identified.
    if ("hcpstruct_zip" in gear_args.common.keys()) or gear_args.fw_specific["gear_dry_run"]:
        # Functional analysis
        if any("fmri" in arg.lower() for arg in [gear_args.common["stages"]]) and (e_code == 0):
            e_code += func_main.run(gear_args, bids_info.layout)

        # Diffusion analysis
        if any(arg in ["dwi", "diffusion"] for arg in [x.lower() for x in gear_args.common["stages"].split(" ")]) and (
            e_code == 0
        ):
            e_code += diff_main.run(gear_args)

    return e_code


if __name__ == "__main__":
    # Decide which env is available
    use_singularity = tmp_singularity.check_for_singularity()
    # To test within a Singularity container, use "(config_path='/flywheel/v0/config.json')" for context.
    # Get access to gear config, inputs, and sdk client if enabled.
    with GearToolkitContext() as gear_context:
        # Initialize logging, set logging level based on `debug` configuration
        # key in gear config.
        gear_context.init_logging()

        # Install the FS license:
        # We want to write it to a folder we know for sure we have write access, in
        # case we are in a singularity environment. Then, set the FS_LICENSE env
        # variable to point to that file.
        freesurfer_license_path = "/tmp/freesurfer_license.txt"
        install_freesurfer_license(gear_context, Path(freesurfer_license_path))
        os.environ["FS_LICENSE"] = freesurfer_license_path

        if use_singularity:
            if not gear_context.config["writable_dir"]:
                log.critical(
                    "HPC usage requires that a writable directory be specified in the configuration tab.\n"
                    "Please add the path to the writable directory and try again."
                )
            gear_name = gear_context.manifest["name"]
            updated_wrtbl_dir = tmp_singularity.start_singularity(
                f"{gear_name}_{gear_context.destination['id']}",
                Path(gear_context.config["writable_dir"]),
                gear_context.config["debug"],
            )
            gear_context._work_dir = updated_wrtbl_dir / "work"
            if not gear_context.work_dir.exists():
                gear_context.work_dir.mkdir(parents=True)
            gear_context._out_dir = updated_wrtbl_dir / "output"
            if not gear_context.output_dir.exists():
                gear_context.output_dir.mkdir(parents=True)
            gear_context.config["writable_dir"] = updated_wrtbl_dir
            tmp_freesurfer_utils.setup_freesurfer_for_singularity(
                Path(updated_wrtbl_dir),
                previous_results=gear_context.get_input_path("hcpstruct_zip"),
            )
        # Pass the gear context into main function defined above.
        main_e_code = main(gear_context)

    # Clean up the space, even/especially if there were errors.
    if use_singularity:
        tmp_singularity.unlink_gear_mounts(gear_context.config["writable_dir"])

    if main_e_code >= 1:
        sys.exit(1)
    else:
        sys.exit(0)
